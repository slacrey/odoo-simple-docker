# Docker Compose starting

```
version: '3.1'
services:
  web:
    image: slacrey/simple:oc-e15.0
    depends_on:
      - db
    ports:
      - "8069:8069"
    volumes:
      - odoo-web-data:/var/lib/odoo
      - ./config:/etc/odoo
      - ./addons:/mnt/extra-addons
      - ./sources:/opt/odoo/sources
    environment:
      - PASSWORD_FILE=/run/secrets/postgresql_password
    secrets:
      - postgresql_password
  db:
    image: postgres:13
    environment:
      - POSTGRES_DB=postgres
      - POSTGRES_PASSWORD_FILE=/run/secrets/postgresql_password
      - POSTGRES_USER=odoo
      - PGDATA=/var/lib/postgresql/data/pgdata
    volumes:
      - odoo-db-data:/var/lib/postgresql/data/pgdata
    secrets:
      - postgresql_password
volumes:
  odoo-web-data:
  odoo-db-data:

secrets:
  postgresql_password:
    file: odoo_pg_pass
```

## 创建odoo_pg_pass文件，存储密码
```
touch odoo_pg_pass
vi odoo_pg_pass
```
## 创配置目录
```
mkdir -p config adons sources 
```

## 下载源码到sources目录
```
cd ./sources
git clone --depth=1 https://gitee.com/slacrey/simple.git -b e.15 .
```

## 服务启动
```
docker-compose up -d
```

## 服务停止
```
docker-compose down
```


