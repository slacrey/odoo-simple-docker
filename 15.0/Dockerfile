FROM ubuntu:20.04
MAINTAINER Simple S.A. <237242115@qq.com>

SHELL ["/bin/bash", "-xo", "pipefail", "-c"]

# Generate locale C.UTF-8 for postgres and general locale data
ENV LANG C.UTF-8

# Set timezone to UTC
RUN ln -sf /usr/share/zoneinfo/Etc/UTC /etc/localtime

COPY ./sources.list /tmp/sources.list

RUN cp /etc/apt/sources.list /etc/apt/sources.list.tmp \
  && rm -rf cp /etc/apt/sources.list \
  && cp /tmp/sources.list /etc/apt/sources.list \
  && rm -rf /tmp/sources.list

# Generate locales
RUN apt-get update \
  && apt-get -yq install locales \
  && locale-gen en_US.UTF-8 \
  && update-locale LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8

# Install some deps, lessc and less-plugin-clean-css, and wkhtmltopdf
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        ca-certificates \
        curl \
        dirmngr \
        fonts-noto-cjk \
        gnupg \
        libssl-dev \
        node-less \
        git \
        npm \
        gcc \
        g++ \
        python3-num2words \
        python3-pdfminer \
        python3-pip \
        python3-phonenumbers \
        python3-pyldap \
        python3-qrcode \
        python3-renderpm \
        python3-setuptools \
        python3-slugify \
        python3-vobject \
        python3-watchdog \
        python3-xlrd \
        python3-xlwt \
        xz-utils \
    && curl -o /tmp/wkhtmltox.deb -sSL https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.5/wkhtmltox_0.12.5-1.focal_amd64.deb \
    && apt-get install -y --no-install-recommends /tmp/wkhtmltox.deb \
    && rm -rf /tmp/wkhtmltox.deb

# Install postgresql postgresql-client
RUN apt-get update && apt-get install -yq lsb-release
RUN curl https://apt.postgresql.org/pub/repos/apt/ACCC4CF8.asc | apt-key add -
RUN sh -c 'echo "deb https://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
RUN apt-get update && apt-get install -yq postgresql-client

# install some dependencies
RUN apt-get update && apt-get install -y python3-dev libxml2-dev libxslt1-dev libldap2-dev libsasl2-dev \
    libtiff5-dev libjpeg8-dev libopenjp2-7-dev zlib1g-dev libfreetype6-dev \
    liblcms2-dev libwebp-dev libharfbuzz-dev libfribidi-dev libxcb1-dev libpq-dev

# mkdir /var/lib/odoo to allow restoring filestore and /mnt/extra-addons for users addons
RUN mkdir -p /mnt/extra-addons \
	&& mkdir -p /etc/odoo \
	&& mkdir -p /opt/odoo/sources \
	&& mkdir -p /var/lib/odoo


# Copy entrypoint script and Odoo configuration file
COPY wait-for-psql.py /usr/local/bin/wait-for-psql.py
COPY ./entrypoint.sh /
COPY ./odoo.conf /etc/odoo/

RUN chmod +x /entrypoint.sh \
    && chmod +x /usr/local/bin/wait-for-psql.py

# Define build constants
ENV GIT_BRANCH=15.0

# Add Odoo sources and remove .git folder in order to reduce image size
# https://github.com/parsechina/odoo.git
RUN cd /opt/odoo/sources \
    && git clone --depth=1 https://gitee.com/slacrey/simple.git -b $GIT_BRANCH . \
    && rm -rf .git

# Install Odoo python dependencies
RUN pip3 install setuptools wheel \
  && pip3 install -r /opt/odoo/sources/requirements.txt

# Install rtlcss (on Debian buster)
RUN npm install -g rtlcss


RUN rm -rf /etc/apt/sources.list \
  && cp /etc/apt/sources.list.tmp /etc/apt/sources.list

# Expose Odoo services
EXPOSE 8069 8071 8072

# Set the default config file
ENV ODOO_RC /etc/odoo/odoo.conf


# Create the odoo user
RUN useradd --create-home --home-dir /opt/odoo --no-log-init odoo
RUN chmod -R 775 /opt/odoo \
    && chown -R odoo:odoo /opt/odoo \
    && chown -R odoo:odoo /etc/odoo \
    && chown -R odoo:odoo /mnt/extra-addons \
    && chown -R odoo:odoo /var/lib/odoo

VOLUME ["/var/lib/odoo", "/mnt/extra-addons", "/opt/odoo/sources", "/etc/odoo"]

WORKDIR /opt/odoo/sources

# Set default user when running the container
USER odoo

ENTRYPOINT ["/entrypoint.sh"]
CMD ["odoo"]
